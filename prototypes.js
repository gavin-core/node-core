'use strict'

if (!String.prototype.toCamelCase) {
  String.prototype.toCamelCase = function (splitter) {
    let result = ''
    const parts = this.toLowerCase().split('_')

    for (let i = 0; i < parts.length; i++) {
      if (!i) {
        result += parts[i]
      } else {
        result += parts[i][0].toUpperCase() + parts[i].substring(1)
      }
    }

    return result
  }
}

if (!String.prototype.unCamelCase) {
  String.prototype.unCamelCase = function (splitter, caseSens) {
    let result = ''

    for (let i = 0; i < this.length; i++) {
      if (!isNaN(this[i] * 1) || this[i] === this[i].toLowerCase()) {
        result += this[i]
      } else {
        result += splitter + this[i]
      }
    }

    if (!caseSens) {
      return result
    }
    if (caseSens > 0) {
      return result.toUpperCase()
    }
    return result.toLowerCase()
  }
}

if (!('callInSequence' in Array)) {
  Array.prototype.callInSequence = function (finalCB, data) {
    finalCB = finalCB || function () {}
    let idx = 0
    const _this = this
    data = data || {}

    ;(function _callItem () {
      if (idx === _this.length) {
        return finalCB(null, data)
      }

      _this[idx++](function (err) {
        if (err) {
          return finalCB(err)
        }
        _callItem()
      }, data)
    })()

    return this
  }
}

/* -------------------------------------------------------------- */
/* ----------- Override default javascript functions ------------ */
global.setInterval = function (cb, duration) {
  let baseline
  let nextTick
  const id = new Date().getTime()

  global.intervals = global.intervals || {}
  global.intervals[id] = global.intervals[id] || []

  ;(function tick () {
    if (baseline) {
      baseline += duration
      cb()
    } else {
      baseline = new Date().getTime()
    }

    nextTick = duration - (new Date().getTime() - baseline)
    if (nextTick < 0) {
      nextTick = 0
    }

    if (!global.intervals[id]) {
      return
    }

    global.intervals[id].push(setTimeout(tick, nextTick))

    if (global.intervals[id].length > 2) {
      global.intervals[id].splice(0, 1)
    }
  })()

  return id
}

global.clearInterval = function (id) {
  const interval = global.intervals[id]
  if (!interval) {
    return
  }

  delete global.intervals[id]
  for (let i = interval.length - 1; i >= 0; i--) {
    clearTimeout(interval[i])
  }
}

/* --------- End Override default javascript functions ---------- */
/* -------------------------------------------------------------- */

Function.prototype.callApply = function callApply () {
  // the 'arguments' object that is accessible inside of a
  // function IS NOT an array, and will therefore be handled
  // as an object

  const context = arguments[0]
  const result = []

  for (let i = 1; i < arguments.length; i++) {
    if (typeof arguments[i] !== 'object') {
      result.push(arguments[i])
      continue
    }

    let valid = true
    let initial = true

    for (let name in arguments[i]) {
      if (!valid) {
        break
      }

      if (arguments[i].hasOwnProperty(name)) {
        if (initial && name !== '0') {
          result.push(arguments[i])
          valid = false
          break
        }

        initial = false
        result.push(arguments[i][name])
      }
    }
  }

  return this.apply(context, result)
}

if (!String.prototype.toBool) {
  String.prototype.toBool = function (defaultValue) {
    defaultValue = !!defaultValue || false

    if (this === null || this === undefined) {
      return defaultValue
    }

    if (this === '1' || this === 'true') {
      return true
    }

    if (this === '0' || this === 'false') {
      return false
    }

    return defaultValue
  }
}

/*
if (!Object.prototype.toBool)
  Object.prototype.toBool = function(defaultValue){
    defaultValue = !!defaultValue || false

    if (this === null) {
      return defaultValue
    }

    if (this === true || this === 1 || this === '1' || this === 'true') {
      return true
    }

    if (this === false || this === 0 || this === '0' || this === 'false') {
      return false
    }

    return defaultValue
}
*/

Number.prototype.formatNum = function (format) {
  return this.toString().formatNum(format)
}

String.prototype.formatNum = function (format) {
  if (format.length <= this.length) {
    return this
  }

  let result = '' + this

  for (let i = format.length - this.length; i > 0; i--) {
    result = format.substring(i, i + 1) + result
  }

  return result
}
