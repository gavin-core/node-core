'use strict'

require('./prototypes')
require('colors')

module.exports = {
  config: require('./lib/config'),
  dates: require('./lib/dates'),
  logger: require('./lib/logger'),
  numbers: require('./lib/numbers'),
  pathUtils: require('./lib/path-utils'),
  strings: require('./lib/strings'),
  Version: require('./lib/version'),
  _: require('underscore')
}
