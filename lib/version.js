const _ = require('underscore')

const Version = function (version, _char) {
  if (version instanceof Version) {
    return version
  }

  const _this = this
  let _version

  const _format = (version, _char_) => {
    const char_ = _char_ || '*'
    if (typeof version === 'number') {
      version = version.toString()
    }

    if (version instanceof Version) {
      version = version.toString()
    }

    if (typeof version !== 'string') {
      throw new Error(`Version provided not in the correct format: ${version}`)
    }

    version = version.replace('*', char_.toString())

    const parts = version.split('.')

    switch (parts.length) {
      case 1:
        version += '.' + char_
        // do NOT break here
      case 2:
        version += '.' + _char_
      case 3:
        break
      default:
        throw new Error(`Version provided not in the correct format: ${version}`)
    }

    return version
  }

  const _incrementPart = (index, version, amount) => {
    const parts = version.split('.')
    if (parts[index] !== '0' && !+parts[index]) {
      throw new Error(`Version provided not in the correct format: ${version}`)
    }

    parts[index] = +parts[index] + amount
    return parts.join('.')
  }

  _this.incrementMajor = amount => {
    _version = _incrementPart(0, _version, amount)
    return _this
  }

  _this.incrementMinor = amount => {
    _version = _incrementPart(1, _version, amount)
    return _this
  }

  _this.incrementBuild = amount => {
    _version = _incrementPart(2, _version, amount)
    return _this
  }

  _this.isLessThan = version => {
    if (_this.matches(version)) {
      return false
    }

    version = _format(version, '0')

    const thisParts = _version.split('.')
    const versionParts = version.split('.')

    for (let i = 0; i < thisParts.length; i++) {
      if (+thisParts[i] < +versionParts[i]) {
        return true
      }
      if (+thisParts[i] > +versionParts[i]) {
        return false
      }
    }

    return false
  }

  _this.isMoreThan = version => !_this.matches(version) && !_this.isLessThan.apply(_this.isLessThan, arguments)

  // true/false test on a single version
  _this.matches = version => !!_this.match([version]).length

  // returns an array of versions from an array provided that matches the version
  _this.match = versions => {
    var result = []

    for (let i = 0; i < versions.length; i++) {
      if (_version === versions[i] || new RegExp('\\^' + _version + '\\$').test(new RegExp('\\^' + _format(versions[i], '0') + '\\$'))) {
        result.push(versions[i])
      }
    }

    return result
  }

  // returns a single value/null where the highest value in the array specified matches the version
  _this.maxMatch = versions => {
    const matches = _this.match(versions)

    if (!matches || !matches.length) {
      return null
    }

    return _.sortBy(matches, (v1, v2) => {
      return _format(v1, '0') < _format(v2, '0')
    })[0]
  }

  _this.isSamePart = (version, partname) => {
    if (!version) {
      throw new Error('Invalid version supplied: Version.isSamePart(version, partname)')
    }

    if (!partname) {
      throw new Error('Invalid partname supplied: Version.isSamePart(version, partname)')
    }

    version = new Version(version)

    switch (partname.toString()) {
      case '3':
      case 'build':
        return _this.matches(version)
      case '2':
      case 'minor':
        if (_this.minor() !== +version.minor()) {
          return false
        }
      case '1':
      case 'major':
        if (_this.major() !== +version.major()) {
          return false
        }
        break
      default:
        throw new Error('Invalid partname supplied')
    }

    return true
  }

  _this.major = () => +_version.split('.')[0]
  _this.minor = () => +_version.split('.')[1]
  _this.build = () => +_version.split('.')[2]

  _this.toString = () => '' + _version

  _version = _format(version, _char)

  return _this
}

Version.prototype.indexOf = function () {
  const res = this.toString()
  return res.indexOf.apply(res.indexOf, arguments)
}

module.exports = Version
