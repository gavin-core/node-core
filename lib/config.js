const _ = require('underscore')
const path = require('path')

const _load = dir => {
  const config = {}
  const parts = dir.split(path.sep)
  let i = 1

  while (i <= parts.length) {
    let directory = parts.slice(0, i).join(path.sep)
    directory = path.resolve(directory, 'config.json')

    try {
      _.extend(config, require(directory))
    } catch (e) {
    } finally {
      i++
    }
  }

  config.__ = {
    load: _load
  }
  _.extend(module.exports, config)
}

module.exports = {}
_load(require.main.filename.substring(0, require.main.filename.lastIndexOf(path.sep)))
