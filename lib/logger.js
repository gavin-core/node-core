const events = require('events')
const config = require('./config')

let _context = ''
const _logLevels = {
  error: 0,
  none: 1,
  info: 2,
  warning: 3,
  verbose: 4,
  debug: 5
}
let _logLevel = _logLevels[config.loglevel] !== undefined ? _logLevels[config.loglevel] : _logLevels.info

let _log

const _setContext = context => {
  _context = context
}

const _setLogLevel = level => {
  _logLevel = level
}

const _logError = (obj, context) => {
  _log(_logLevels.error, context || _context, obj)
}

const _logWarning = (obj, context) => {
  _log(_logLevels.warning, context || _context, obj)
}

const _logNone = (obj, context) => {
  _log(_logLevels.none, context || _context, obj)
}

const _logDebug = (obj, context) => {
  _log(_logLevels.debug, context || _context, obj)
}

const _logVerbose = (obj, context) => {
  _log(_logLevels.verbose, context || _context, obj)
}

const _logInfo = (obj, context) => {
  _log(_logLevels.info, context || _context, obj)
}

const Logger = function () {
  const _this = this

  _this.LogLevels = _logLevels
  _this.LogLevel = _logLevel

  _this.setContext = _setContext
  _this.setLogLevel = _setLogLevel
  _this.error = _logError
  _this.warning = _logWarning
  _this.capture = _logNone
  _this.info = _logInfo
  _this.verbose = _logVerbose
  _this.debug = _logDebug

  _this.log = _log = (level, context, obj) => {
    if (!obj) {
      obj = context
      context = null
    }

    if (_logLevel < level) {
      return
    }

    if (context) {
      obj._context = context
    }

    _this.getLogLevelColor(level, (err, color) => {
      if (err) {
        console.log('Error received but not catered for')
        console.error(err)
        return
      }

      console.log(obj.toString()[color])
    })
  }

  _this.getLogLevelColor = (logLevel, cb) => {
    switch (logLevel) {
      case _this.LogLevels.error:
        return cb(null, 'red')
      case _this.LogLevels.none:
        return cb(null, 'green')
      case _this.LogLevels.warning:
        return cb(null, 'yellow')
      case _this.LogLevels.info:
        return cb(null, 'cyan')
      case _this.LogLevels.verbose:
        return cb(null, 'magenta')
      case _this.LogLevels.debug:
        return cb(null, 'gray')
      default:
        return cb(null, 'white')
    }
  }

  return _this
}

Logger.prototype = events.EventEmitter.prototype
module.exports = new Logger()
module.exports.__logger = Logger
