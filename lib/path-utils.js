const path = require('path')
const fs = require('fs')

module.exports = {
  ensureExists: function () {
    let dirPath
    let mode = 484 // 484 === 0777 in decimal
    let cb

    switch (arguments.length) {
      case 1:
        dirPath = arguments[0]
        break
      case 2:
        dirPath = arguments[0]
        cb = arguments[1]
        break
      case 3:
        dirPath = arguments[0]
        mode = arguments[1]
        cb = arguments[2]
        break
      default:
        throw new Error('A path and a callback is required for path-utils.ensureExists')
    }

    if (typeof cb !== 'function') {
      cb = () => {}
    }

    const sep = dirPath.indexOf('/') >= 0 ? '/' : '\\'
    const directories = dirPath.split(sep)
    const ensureExists = index => (next, data) => {
      let dPath = sep === '/' ? '/' : ''
      for (let i = 0; i <= index; i++) {
        dPath += directories[i] + path.sep
      }

      data.path = dPath

      fs.stat(dPath, (err, stat) => {
        if (!err && stat.isDirectory()) {
          return next()
        }

        fs.mkdir(dPath, mode, next)
      })
    }

    if (directories[0] === '') {
      directories.shift()
    }

    const arr = []
    for (let i = 0; i < directories.length; i++) {
      if ((i + 1) === directories.length && directories[i].indexOf('.') >= 0) {
        continue
      }

      arr.push(ensureExists(i))
    }

    arr.callInSequence((err, data) => {
      if (err) {
        return cb(err)
      }

      cb(err, data.path)
    }, { path: '' })
  }
}
