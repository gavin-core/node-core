module.exports = {
  lowerCaseFirstChar: str => str.charAt(0).toLowerCase() + str.slice(1),
  leftPad: (_str, width) => {
    const str = _str.trim()
    let result = ''

    for (let j = str.length; j < width; j++) {
      result += ' '
    }

    result += str

    return result
  }
}
