const moment = require('moment')

module.exports = {
  FORMATS: {
    SHORT_DATE: 'DD MMM YYYY',
    SHORT_DATE_TIME: 'DD MMM YYYY, h:mm a',
    LONG_DATE: 'dddd DD MMM YYYY',
    LONG_DATE_TIME: 'dddd DD MMM YYYY, , h:mm a'
  },
  formatAsDefaultDate: date => moment(date).format(module.exports.FORMATS.SHORT_DATE),
  formatAsDefaultDateTime: date => moment(date).format(module.exports.FORMATS.SHORT_DATE_TIME),
  formatAsLongDate: date => moment(date).format(module.exports.FORMATS.LONG_DATE),
  formatAsLongDateTime: date => moment(date).format(module.exports.FORMATS.LONG_DATE_TIME)
}
